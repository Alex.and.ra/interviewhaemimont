package com.interview.interview.models;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "students_courses")
public class StudentCourse {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="student_course_id")
    private int studentCourseId;

    @ManyToOne
    @JoinColumn(name = "student_pin")
    private Student student;

    @ManyToOne
    @JoinColumn(name = "course_id")
    private Course course;

    @JoinColumn(name = "completion_date")
    private LocalDate completionDate;

    public StudentCourse() {
    }

    public StudentCourse(int courseId, Student student, Course course, LocalDate completionDate) {
        this.studentCourseId = courseId;
        this.student = student;
        this.course = course;
        this.completionDate = completionDate;
    }

    public int getStudentCourseId() {
        return studentCourseId;
    }

    public void setStudentCourseId(int studentCourseId) {
        this.studentCourseId = studentCourseId;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public LocalDate getCompletionDate() {
        return completionDate;
    }

    public void setCompletionDate(LocalDate completionDate) {
        this.completionDate = completionDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StudentCourse that = (StudentCourse) o;
        return studentCourseId == that.studentCourseId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(studentCourseId);
    }
}
